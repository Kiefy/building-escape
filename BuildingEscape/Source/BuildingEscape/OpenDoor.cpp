// Copyright 2018 Kief Inc

#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "Components/PrimitiveComponent.h"

#define OUT

UOpenDoor::UOpenDoor()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	DoorActor = GetOwner();

	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("PressurePlate not defined!"));
	}
}


void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (TotalMassOfActorsOnPlate() > MassRequiredToOpenDoor)
	{
		OpenDoor();
	}
	else
	{
		CloseDoor();
	}
}


float UOpenDoor::TotalMassOfActorsOnPlate()
{
	float TotalMass = 0.f;
	if (PressurePlate)
	{
		TArray<AActor*> OverlappingActors;
		PressurePlate->GetOverlappingActors(OUT OverlappingActors);
		for (const auto* actor : OverlappingActors)
		{
			TotalMass += actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		}
	}
	return TotalMass;
}


void UOpenDoor::OpenDoor()
{
	//DoorActor->SetActorRotation(FRotator(0.f, OpenAngle, 0.f));
	OnOpen.Broadcast();
	LastDoorOpenTime = GetWorld()->GetTimeSeconds();
}


void UOpenDoor::CloseDoor()
{
	if (GetWorld()->GetTimeSeconds() - LastDoorOpenTime > DoorCloseDelay)
	{
		//DoorActor->SetActorRotation(FRotator(0.f, 0.f, 0.f));
		OnClose.Broadcast();

	}
}
